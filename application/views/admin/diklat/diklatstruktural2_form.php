<table class="table table-striped">
<form action="" method="POST" enctype="multipart/form-data"> 
	<tr><th>Nip</th><td><input type="text" name="nip" class="form-control" value="<?= $nip ?>" required=""></td><th class="text-danger" >***</th></tr>
	<tr><th>Nama Pegawai</th><td><input type="text" name="nama" class="form-control" value="<?= $nama ?>" required=""></td><th class="text-danger" >***</th></tr>
	<tr><th>Nama DiklatI</th><td><input type="text" name="diklatI" class="form-control" value="<?= $diklatI ?>" required=""></td><th class="text-danger" >***</th></tr>
	<tr><th>Jam DiklatI</th><td><input type="text" name="jam_diklatI" class="form-control" value="<?= $jam_diklatI ?>"></td></tr>
	<tr><th>Tanggal DiklatI</th><td><input type="text" name="tgl_diklatI" class="form-control" value="<?= $tgl_diklatI ?>"></td></tr>
	<tr><th>Tahun DiklatI</th><td><input type="text" name="tahun_diklatI" class="form-control" value="<?= $tahun_diklatI ?>"></td></tr>
	<tr><th>Angkatan DiklatI</th><td><input type="text" name="angkatan_diklatI" class="form-control" value="<?= $angkatan_diklatI ?>"></td></tr>
	<tr><th>No DiklatI</th><td><input type="text" name="no_diklatI" class="form-control" value="<?= $no_diklatI ?>"></td></tr>
	<tr><th>Penyelenggara DiklatI</th><td><input type="text" name="penyelenggara_diklatI" class="form-control" value="<?= $penyelenggara_diklatI ?>"></td></tr>
	<tr><th>Tempat DiklatI</th><td><input type="text" name="tempat_diklatI" class="form-control" value="<?= $tempat_diklatI ?>"></td></tr>

	<tr><th>Nama DiklatII</th><td><input type="text" name="diklatII" class="form-control" value="<?= $diklatII ?>" required=""></td><th class="text-danger" >***</th></tr>
	<tr><th>Jam DiklatII</th><td><input type="text" name="jam_diklatII" class="form-control" value="<?= $jam_diklatII ?>"></td></tr>
	<tr><th>Tanggal DiklatII</th><td><input type="text" name="tgl_diklatII" class="form-control" value="<?= $tgl_diklatII ?>"></td></tr>
	<tr><th>Tahun DiklatII</th><td><input type="text" name="tahun_diklatII" class="form-control" value="<?= $tahun_diklatII ?>"></td></tr>
	<tr><th>Angkatan DiklatII</th><td><input type="text" name="angkatan_diklatII" class="form-control" value="<?= $angkatan_diklatII ?>"></td></tr>
	<tr><th>No DiklatII</th><td><input type="text" name="no_diklatII" class="form-control" value="<?= $no_diklatII ?>"></td></tr>
	<tr><th>Penyelenggara DiklatII</th><td><input type="text" name="penyelenggara_diklatII" class="form-control" value="<?= $penyelenggara_diklatII ?>"></td></tr>
	<tr><th>Tempat DiklatII</th><td><input type="text" name="tempat_diklatII" class="form-control" value="<?= $tempat_diklatII ?>"></td></tr>

	<tr><th>Nama DiklatIII</th><td><input type="text" name="diklatIII" class="form-control" value="<?= $diklatIII ?>" required=""></td><th class="text-danger" >***</th></tr>
	<tr><th>Jam DiklatIII</th><td><input type="text" name="jam_diklatIII" class="form-control" value="<?= $jam_diklatIII ?>"></td></tr>
	<tr><th>Tanggal DiklatIII</th><td><input type="text" name="tgl_diklatIII" class="form-control" value="<?= $tgl_diklatIII ?>"></td></tr>
	<tr><th>Tahun DiklatIII</th><td><input type="text" name="tahun_diklatIII" class="form-control" value="<?= $tahun_diklatIII ?>"></td></tr>
	<tr><th>Angkatan DiklatIII</th><td><input type="text" name="angkatan_diklatIII" class="form-control" value="<?= $angkatan_diklatIII ?>"></td></tr>
	<tr><th>No DiklatIII</th><td><input type="text" name="no_diklatIII" class="form-control" value="<?= $no_diklatIII ?>"></td></tr>
	<tr><th>Penyelenggara DiklatIII</th><td><input type="text" name="penyelenggara_diklatIII" class="form-control" value="<?= $penyelenggara_diklatIII ?>"></td></tr>
	<tr><th>Tempat DiklatIII</th><td><input type="text" name="tempat_diklatIII" class="form-control" value="<?= $tempat_diklatIII ?>"></td></tr>

	<tr><th>Nama DiklatIV</th><td><input type="text" name="diklatIV" class="form-control" value="<?= $diklatIV ?>" required=""></td><th class="text-danger" >***</th></tr>
	<tr><th>Jam DiklatIV</th><td><input type="text" name="jam_diklatIV" class="form-control" value="<?= $jam_diklatIV ?>"></td></tr>
	<tr><th>Tanggal DiklatIV</th><td><input type="text" name="tgl_diklatIV" class="form-control" value="<?= $tgl_diklatIV ?>"></td></tr>
	<tr><th>Tahun DiklatIV</th><td><input type="text" name="tahun_diklatIV" class="form-control" value="<?= $tahun_diklatIV ?>"></td></tr>
	<tr><th>Angkatan DiklatIV</th><td><input type="text" name="angkatan_diklatIV" class="form-control" value="<?= $angkatan_diklatIV ?>"></td></tr>
	<tr><th>No DiklatIV</th><td><input type="text" name="no_diklatIV" class="form-control" value="<?= $no_diklatIV ?>"></td></tr>
	<tr><th>Penyelenggara DiklatIV</th><td><input type="text" name="penyelenggara_diklatIV" class="form-control" value="<?= $penyelenggara_diklatIV ?>"></td></tr>
	<tr><th>Tempat DiklatIV</th><td><input type="text" name="tempat_diklatIV" class="form-control" value="<?= $tempat_diklatIV ?>"></td></tr>

<?php
	$disabled = "";
	if($aksi == "edit"){
		$disabled = "disabled";
	}
?>
<tr><td></td><th><input type="submit" name="kirim" value="Submit" class="btn btn-primary"> <a href="http://localhost/simpeg_pupr/admin/diklatstruktural" class="btn btn-success" class="small-box-footer">Back <i class="fa fa-arrow-circle-right"></i></a></th></tr>

</form>
</table>
<?php 
if($aksi == "edit"):
?>	
<span><i></i></span>
<?php endif; ?>
