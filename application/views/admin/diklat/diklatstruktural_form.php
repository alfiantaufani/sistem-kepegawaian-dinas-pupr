<table class="table table-striped">
<a href="#"> <button onClick="window.print();" class="btn btn-warning"><i class="fa fa-print"></i>Print Data</button></a>
<form action="" method="POST" enctype="multipart/form-data"> 
<table id="example1" class="table table-striped table-bordered">
	<tr>
	<td width="500">NIP</td>
	<td width="70" text align="center">:</td>
	<td><?= $nip ?></td>
	</tr>
	
	<tr>
	<td>Nama</td>
	<td width="70" text align="center">:</td>
	<td><?= $nama ?></td>
	</tr>
	
	<tr>
	<td>DiklatI</td>
	<td width="70" text align="center">:</td>
	<td><?= $diklatI ?></td>
	</tr>
	
	<tr>
	<td>Jam DiklatI</td>
	<td width="70" text align="center">:</td>
	<td><?= $jam_diklatI ?></td>
	</tr>
	
	<tr>
	<td>Tanggal DiklatI</td>
	<td width="70" text align="center">:</td>
	<td><?= $tgl_diklatI ?></td>
	</tr>
	
	<tr>
	<td>Tahun DiklatI</td>
	<td width="70" text align="center">:</td>
	<td><?= $tahun_diklatI ?></td>
	</tr>
	
	<tr>
	<td>Angkatan DiklatI</td>
	<td width="70" text align="center">:</td>
	<td><?= $angkatan_diklatI ?></td>
	</tr>
	
	<tr>
	<td>No DiklatI</td>
	<td width="70" text align="center">:</td>
	<td><?= $no_diklatI ?></td>
	</tr>
	
	<tr>
	<td>Penyelenggara DiklatI</td>
	<td width="70" text align="center">:</td>
	<td><?= $penyelenggara_diklatI ?></td>
	</tr>
	
	<tr>
	<td>Tempat DiklatI</td>
	<td width="70" text align="center">:</td>
	<td><?= $tempat_diklatI ?></td>
	</tr>

	<tr>
	<td>DiklatII</td>
	<td width="70" text align="center">:</td>
	<td><?= $diklatII ?></td>
	</tr>
	
	<tr>
	<td>Jam DiklatII</td>
	<td width="70" text align="center">:</td>
	<td><?= $jam_diklatII ?></td>
	</tr>
	
	<tr>
	<td>Tanggal DiklatII</td>
	<td width="70" text align="center">:</td>
	<td><?= $tgl_diklatII ?></td>
	</tr>
	
	<tr>
	<td>Tahun DiklatII</td>
	<td width="70" text align="center">:</td>
	<td><?= $tahun_diklatII ?></td>
	</tr>
	
	<tr>
	<td>Angkatan DiklatII</td>
	<td width="70" text align="center">:</td>
	<td><?= $angkatan_diklatII ?></td>
	</tr>
	
	<tr>
	<td>No DiklatII</td>
	<td width="70" text align="center">:</td>
	<td><?= $no_diklatII ?></td>
	</tr>
	
	<tr>
	<td>Penyelenggara DiklatII</td>
	<td width="70" text align="center">:</td>
	<td><?= $penyelenggara_diklatII ?></td>
	</tr>
	
	<tr>
	<td>Tempat DiklatII</td>
	<td width="70" text align="center">:</td>
	<td><?= $tempat_diklatII ?></td>
	</tr>

	<tr>
	<td>DiklatIII</td>
	<td width="70" text align="center">:</td>
	<td><?= $diklatIII ?></td>
	</tr>
	
	<tr>
	<td>Jam DiklatIII</td>
	<td width="70" text align="center">:</td>
	<td><?= $jam_diklatIII ?></td>
	</tr>
	
	<tr>
	<td>Tanggal DiklatIII</td>
	<td width="70" text align="center">:</td>
	<td><?= $tgl_diklatIII ?></td>
	</tr>
	
	<tr>
	<td>Tahun DiklatIII</td>
	<td width="70" text align="center">:</td>
	<td><?= $tahun_diklatIII ?></td>
	</tr>
	
	<tr>
	<td>Angkatan DiklatIII</td>
	<td width="70" text align="center">:</td>
	<td><?= $angkatan_diklatIII ?></td>
	</tr>
	
	<tr>
	<td>No DiklatIII</td>
	<td width="70" text align="center">:</td>
	<td><?= $no_diklatIII ?></td>
	</tr>
	
	<tr>
	<td>Penyelenggara DiklatIII</td>
	<td width="70" text align="center">:</td>
	<td><?= $penyelenggara_diklatIII ?></td>
	</tr>
	
	<tr>
	<td>Tempat DiklatIII</td>
	<td width="70" text align="center">:</td>
	<td><?= $tempat_diklatIII ?></td>
	</tr>

	<tr>
	<td>DiklatIV</td>
	<td width="70" text align="center">:</td>
	<td><?= $diklatIV ?></td>
	</tr>
	
	<tr>
	<td>Jam DiklatIV</td>
	<td width="70" text align="center">:</td>
	<td><?= $jam_diklatIV ?></td>
	</tr>
	
	<tr>
	<td>Tanggal DiklatIV</td>
	<td width="70" text align="center">:</td>
	<td><?= $tgl_diklatIV ?></td>
	</tr>
	
	<tr>
	<td>Tahun DiklatIV</td>
	<td width="70" text align="center">:</td>
	<td><?= $tahun_diklatIV ?></td>
	</tr>
	
	<tr>
	<td>Angkatan DiklatIV</td>
	<td width="70" text align="center">:</td>
	<td><?= $angkatan_diklatIV ?></td>
	</tr>
	
	<tr>
	<td>No DiklatIV</td>
	<td width="70" text align="center">:</td>
	<td><?= $no_diklatIV ?></td>
	</tr>
	
	<tr>
	<td>Penyelenggara DiklatIV</td>
	<td width="70" text align="center">:</td>
	<td><?= $penyelenggara_diklatIV ?></td>
	</tr>
	
	<tr>
	<td>Tempat DiklatIV</td>
	<td width="70" text align="center">:</td>
	<td><?= $tempat_diklatIV ?></td>
	</tr>
	 
<?php
	$disabled = "";
	if($aksi == "edit"){
		$disabled = "disabled";
	}
?>
</form>
</table>
<?php 
if($aksi == "edit"):
?>	
<span><i></i></span>
<?php endif; ?>